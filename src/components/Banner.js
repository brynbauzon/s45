// Bootstrap Components
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


const Banner = () => {
    
    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1>Zuit Coding Bootcamp</h1>
                    <p>Oppurtunities for Everyone, Everywhere</p>
                    <Button variant="primary">Enroll Now</Button>
                </Jumbotron>
            </Col>
        </Row>

    )
}


export default Banner

