// Bootstrap Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button'
import { useEffect, useState } from 'react';


export default function Course(props) {

    let  course = props.course;
    //const [count, setCount] = useState(0)
    const [seats, setSeats] = useState(30)
    const [isDisabled, setIsDisabled] = useState(10)

    // const enroll = () => {
    //         if(seats == 0){
    //             alert('Course is Full!')
    //         }else{

    //             setCount(count + 1)
    //             setSeats(seats - 1)
    //         }
    // }
        

    useEffect(()=>{

        if(seats === 0){

            setIsDisabled(true);   
        
        }
    },[seats])


         return(
      
                <Card>
                <Card.Body>
                    <Card.Title>{course.name}</Card.Title>
                   <h6>Description</h6>
                   <p>{course.description}</p>
                   <h6>Price</h6>
                   <p>Seats</p>
                   <p>{seats} remaining</p>
                   <Button variant="primary" onClick={()=>setSeats(seats-1)} disabled={isDisabled}>Enroll</Button>                    </Card.Body>    
                </Card>
                

   )

}

