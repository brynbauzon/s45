import {useState, useEffect} from 'react';

//Bootstrap componenets
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'



const Login = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isDisabled, setIsDisabled] = useState('');


    
    useEffect(()=>{

        let isEmailIsNotEmpty =  email !== '';
        let isPasswordIsNotEmpty = password !=='';
 
          //Determine if all conditions are met
          if(isEmailIsNotEmpty && isPasswordIsNotEmpty){

              setIsDisabled(false)
          }else{

              setIsDisabled(true)
          }



      },[email, password])

      const login = (e) =>{

        e.preventDefault()
        alert('Login Successful')

        setEmail('');
        setPassword('');
    }

    return(
        <Container fluid>
            <Form onSubmit={login}>
            <Form.Group>
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" required value={email} onChange={(e)=>setEmail(e.target.value)}/>
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" required value={password} onChange={(e)=>setPassword(e.target.value)}/>
            </Form.Group>
            <Button variant="primary" type="submit" disabled={isDisabled}>Login</Button>
            </Form>
        </Container>


    )
}

export default Login;