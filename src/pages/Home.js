// Bootstrap Components
import Container from 'react-bootstrap/Container';

// App Components
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


const Home = () => {

    return (

        <Container Fluid>
            <Banner />
            <Highlights />
        </Container>
    )

}

export default Home