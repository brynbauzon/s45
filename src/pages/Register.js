import {useState, useEffect} from 'react';

//Bootstrap componenets
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import Button from 'react-bootstrap/Button'


const Register = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirm, setPasswordConfirm] = useState('');
    const [isDisabled, setIsDisabled] = useState('');


    // useEffect(()=>{

    // console.log(email)

    // }, [email])

    // useEffect(()=>{

    //     console.log(password)
        
    //     }, [password])

    // useEffect(()=>{

    //     console.log(passwordConfirm)
        
    //     }, [passwordConfirm])





        useEffect(()=>{

          let isEmailIsNotEmpty =  email !== '';
          let isPasswordIsNotEmpty = password !=='';
          let isPasswordConfirmIsNotEmpty = passwordConfirm !=='';
          let isPasswordMatched = password === passwordConfirm;

            //Determine if all conditions are met
            if(isEmailIsNotEmpty && isPasswordIsNotEmpty && isPasswordConfirmIsNotEmpty && isPasswordMatched){

                setIsDisabled(false)
            }else{

                setIsDisabled(true)
            }



        },[email, password, passwordConfirm])


        const register = (e) =>{

            e.preventDefault()
            alert('Registration Successful')
        }


    return (
        <Container fluid>
         <h1>Register</h1>
         <Form onSubmit={register}>
                <Form.Group>
                <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required value={email} onChange={(e)=>setEmail(e.target.value)}/>
                    <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                    </Form.Group>
                    <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required value={password} onChange={(e)=>setPassword(e.target.value)}/>
                    </Form.Group>
                    <Form.Group>
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" required value={passwordConfirm} onChange={(e)=>setPasswordConfirm(e.target.value)}/>
                    </Form.Group>
                    <Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
                    </Form>
          </Container>
    )

}

export default Register;