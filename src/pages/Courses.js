import Course from '../components/Course'

// Bootstrap Components
import Container from 'react-bootstrap/Container'

// Data Imports from mock-database
import courses from '../mock-data/courses'

const Courses = () => {


    //Loading data of arrays
    const CourseCards = courses.map((course) => {

        return(

            <Course course = {course} key={course.id}/>)
    })

    return(
        <Container fluid>
            {CourseCards}
        </Container>
    )
}


export default Courses