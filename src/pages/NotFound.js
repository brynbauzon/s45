
// Bootstrap Components
import {Link} from "react-router-dom";

const NotFound = () => {
	return(
		<>
		<h3>Page Not Found</h3>
		<p>Go back to the <Link to = '/'>homepage</Link>.</p>
		</>
	)
}

export default NotFound