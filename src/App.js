import React from 'react';
import AppNavBar from './components/AppNavBar';
import Course from './components/Course';
import Counter from './components/Counter';


//Base Imports

import ReactDOM from 'react-dom';
import {BrowseROuter, BrowserRouter, Route, Routes } from 'react-router-dom'



import 'bootstrap/dist/css/bootstrap.min.css';
//import './index.css';


//Page Components
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';






const App = () => {

    return (


    <BrowserRouter>
        <AppNavBar />
        <Routes>
            <Route path ="/" element ={<Home />} />
            <Route path = "/courses" element ={<Courses />} />
            <Route path ="/register" element ={<Register />} />
            <Route path ="/login" element ={<Login />} />
            <Route path="*" element = {<NotFound />} />
        </Routes>
        </BrowserRouter>

    )


}

export default App


/// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();